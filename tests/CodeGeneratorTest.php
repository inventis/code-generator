<?php

namespace Inventis\CodeGenerator;

use PHPUnit\Framework\TestCase;

class CodeGeneratorTest extends TestCase
{
    /**
     * @var CodeGenerator
     */
    private $codeGenerator;

    public function patternDataProvider()
    {
        return[
            ['****', '/^[a-zA-Z0-9]{4}$/'],
            ['*-*-*-*', '/^[a-zA-Z0-9]-[a-zA-Z0-9]-[a-zA-Z0-9]-[a-zA-Z0-9]$/'],
            ['**#', '/^[a-zA-Z0-9]{2}[0-9]{1}$/'],
            ['**$(2)', '/^[a-zA-Z0-9]{2}[0-9]{2}$/'],
            ['SD89', '/^SD89/'],
            ['SD-89', '/^SD\-89/'],
        ];
    }

    protected function setUp(): void
    {
        $this->codeGenerator = new CodeGenerator();
    }

    /**
     * @dataProvider patternDataProvider
     *
     * @param string $pattern
     * @param string $matchesRegex
     */
    public function testPatterns(string $pattern, string $matchesRegex)
    {
        $code = $this->codeGenerator->createGenerators($pattern)->generateCode();

        self::assertRegExp($matchesRegex, $code);
    }
}
