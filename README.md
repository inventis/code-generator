# Code Generator
Generate a random string based on a pattern

## Supported patterns
- \#: Generate consecutive numbers, use multiple \#'s for larger consecutive numbers, resets on max
- \*: Generates an Alphanumeric character randomly
- $(2): Generates a time based random number (variant of [TOTP](https://www.google.be/url?sa=t&rct=j&q=&esrc=s&source=web&cd=13&cad=rja&uact=8&ved=0ahUKEwi9rfGBuN7YAhWBKVAKHXD-BrMQFghhMAw&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTime-based_One-time_Password_Algorithm&usg=AOvVaw3ZAd4fc6MeMUPgN3h5wxbb)) with a length of 2
use $(5) for a length of 5 etc...


## Examples

```

// generate 4 alphanumeric characters
$codeGenerator = new CodeGenerator();
$codeGenerator->createGenerators('****');

$code1 = $codeGenerator->generateCode(); // WUDJ
$code2 = $codeGenerator->generateCode(); // IAJH
...

// fixed text + sequensial numbers and alphanumeric combinations
$codeGenerator->createGenerators('HAPPYHOUR-##**#');
// __toString
$code3 = (string) $codeGenerator; // HAPPYHOUR-01XF1
$code4 = (string) $codeGenerator; // HAPPYHOUR-02RC2
...

// construct from pattern at instanciation
$codeGenerator = new CodeGenerator('*-$(6)');
$code5 = (string) $codeGenerator->generateCode(); // D-955467
...


```