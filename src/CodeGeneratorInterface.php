<?php
namespace Inventis\CodeGenerator;

interface CodeGeneratorInterface
{
    /**
     * same as __toString, generates a new code on every call to it
     *
     * @return string
     */
    public function generateCode(): string;

    /**
     * returns the maximum amount of unique codes the generator can generate with the current generators
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int;

    /**
     * generates a new code on every cast to string
     *
     * @return string
     */
    public function __toString(): string;
}
