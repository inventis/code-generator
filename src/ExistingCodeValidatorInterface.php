<?php

namespace Inventis\CodeGenerator;

interface ExistingCodeValidatorInterface
{
    public function generatedCodeExists($code): bool;
}
