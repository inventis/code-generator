<?php
namespace Inventis\CodeGenerator;

class CodeGenerator implements CodeGeneratorInterface
{
    const HASH_GENERATOR = 1;
    const WILDCARD_GENERATOR = 2;
    const STATIC_ALPHANUMERIC_GENERATOR = 4;
    const TIME_BASED_GENERATOR = 8;

    /**
     * a bit map for all generators that this generator will be using during code analysis, the map is a bit as key
     * and PatternGeneratorInterface FQCN as value
     *
     * @var array|PatternGeneratorInterface[]
     */
    protected static $generatorsBitMap = [
        self::HASH_GENERATOR                => HashPatternGenerator::class,
        self::WILDCARD_GENERATOR            => WildcardPatternGenerator::class,
        self::STATIC_ALPHANUMERIC_GENERATOR => StaticAlphaNumericPatternGenerator::class,
        self::TIME_BASED_GENERATOR          => TimePatternGenerator::class,
    ];

    /**
     * @var string
     */
    protected $lastCode;

    /**
     * the by offset ordered list of generators identified
     *
     * @var PatternGeneratorInterface[]
     */
    private $generators = [];

    /**
     * @var int
     */
    private $generatorBits;

    /**
     * @var string
     */
    private $code;

    /**
     * @param null|string $pattern the pattern from which to generate a list of PatternCodeGeneratorInterface objects
     * @param null|int $generatorBits bitwise flag for all generators to use, if null all mapped generators
     *                                will be used
     */
    public function __construct(?string $pattern = null, ?int $generatorBits = null)
    {
        $this->setGeneratorBits($generatorBits);
        if ($pattern !== null) {
            $this->createGenerators($pattern);
        }
    }

    /**
     * set the code used for list generation
     *
     * @param string $code
     * @return $this
     */
    protected function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function getCode(): self
    {
        return $this->code;
    }

    protected function setGeneratorBits(?int $generatorBits)
    {
        if ($generatorBits === null) {
            $generatorBits = static::getDefaultGeneratorBits();
        }
        $this->generatorBits = $generatorBits;
        return $this;
    }

    /**
     * returns the used int flag for to use generators
     *
     * @return int
     */
    public function getGeneratorBits(): int
    {
        return $this->generatorBits;
    }

    /**
     * created generators from the currently defines bits and define them as the list
     * of generators to use for code generation, this is usually only something you'd do once as soon as you know the
     * pattern to use
     *
     * @param string $codePattern the code from which to generate the required generators
     * @return $this
     */
    public function createGenerators(string $codePattern): self
    {
        $this->setCode($codePattern); // should the used want to request it again
        $generatorBits = $this->getGeneratorBits();
        $generators = [];
        foreach (static::$generatorsBitMap as $bit => $className) {
            if ($generatorBits & $bit) {
                $generators = array_merge($generators, $className::createFromCodePattern($codePattern));
            }
        }
        return $this->setGeneratorsList($generators);
    }

    /**
     * returns the default generator bits
     * this method uses all generators registered as the default
     *
     */
    public static function getDefaultGeneratorBits(): int
    {
        return static::getAllGeneratorBits();
    }

    /**
     * generates an array of codes that where created by the generator
     * mapped to the offset for which they apply
     *
     * @return string[]
     */
    public function generateCodes(): array
    {
        $codes = [];
        foreach ($this->getGenerators() as $generator) {
            $codes[$generator->getOffset()] = $generator->generateCode();
        }
        $this->setLastGeneratedCode(implode('', $codes));
        return $codes;
    }

    /**
     * same as __toString, generates a new code on every call to it
     *
     * @return string
     */
    public function generateCode(): string
    {
        return $this->__toString();
    }

    /**
     * set the list of generators
     *
     * @param PatternGeneratorInterface[] $generators
     *
     * @return $this
     */
    protected function setGeneratorsList(array $generators): self
    {
        $this->generators = [];
        foreach ($generators as $generator) {
            $this->addGeneratorToList($generator);
        }
        return $this;
    }

    /**
     * add a generator to the list of code matches generators
     *
     * @param PatternGeneratorInterface $generator
     *
     * @return void
     */
    protected function addGeneratorToList(PatternGeneratorInterface $generator): void
    {
        $offset = $generator->getOffset();
        if (isset($this->generators[$offset])) {
            var_dump($this->generators, $offset);
            throw new DuplicateGeneratorOffsetException(
                '2 Code generators have matches to the same code offset, this means that those generators cannot be ' .
                ' used together as it would result in overlapping codes'
            );
        }
        $this->generators[$offset] = $generator;
        ksort($this->generators);
    }

    /**
     * returns an offset sorted list of generators that where identified and created for the provided $code
     *
     * @return \Inventis\CodeGenerator\PatternGeneratorInterface[]
     */
    public function getGenerators(): array
    {
        return $this->generators;
    }

    /**
     * @param int $bit
     * @param string $className if it does not have a leading \ a namespace relative to the generator is prefixed
     *
     * @return void
     */
    public static function registerGenerator(int $bit, string $className): void
    {
        if (isset(static::$generatorsBitMap)) {
            throw new DuplicateGeneratorBitException('You cannot register 2 generators by the same bit');
        } else {
            if (static::isConflictingBit($bit)) {
                throw new DuplicateGeneratorBitException(
                    'The bit you are trying to register is colliding with existing bits'
                );
            }
        }
        static::$generatorsBitMap[$bit] = $className;
    }

    public static function getAllGeneratorBits(): int
    {
        return array_sum(array_keys(static::$generatorsBitMap));
    }

    public static function isConflictingBit(int $bit): bool
    {
        return static::getAllGeneratorBits() & $bit;
    }

    /**
     * this is a shorthand method that will allow you to directly get a code for a given pattern
     * this however is not what you should use if you want to generate multiple codes from the same
     * pattern as context will get lost and thus incrementing generators will reset
     *
     * @param string     $pattern the pattern to generate a code from
     * @param bool       $asString if you rather have an array of codes with offsets set this to false
     * @param null|int   $generatorBits bitwise flag for the generators to use
     *
     * @return string|array
     */
    public static function generateCodeFromPattern(string $pattern, bool $asString = true, ?int $generatorBits = null)
    {
        $generator = new static($pattern, $generatorBits);
        if ($asString) {
            return (string) $generator;
        }
        return $generator->generateCodes();
    }

    /**
     * generates a new code on every cast to string
     *
     * @return string
     */
    public function __toString(): string
    {
        $this->generateCodes(); //returns array, but stores thats retrievable through lastGeneratedCode
        return $this->getLastGeneratedCode();
    }


    /**
     * returns the maximum amount of unique codes the generator can generate with the current generators
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int
    {
        $max = 1;
        foreach ($this->getGenerators() as $generator) {
            $max *= $generator->getMaxUniqueCodes();
        }
        return $max;
    }

    /**
     * returns the last code that was generated trough any of the generator options
     *
     * @return string
     */
    public function getLastGeneratedCode(): string
    {
        return $this->lastCode;
    }

    /**
     * set the last generated code as a string
     *
     * @param string $code
     *
     * @return $this
     */
    protected function setLastGeneratedCode($code): self
    {
        $this->lastCode = $code;
        return $this;
    }
}
