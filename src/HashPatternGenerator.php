<?php
namespace Inventis\CodeGenerator;

class HashPatternGenerator extends AbstractPatternGenerator
{
    /**
     * match #, ##, ###, ...
     * @var string
     */
    protected static $patternMatcher = '/#+/';

    protected $matchLength;
    protected $lastCode = 0;

    public function __construct(string $pattern, int $offset)
    {
        parent::__construct($pattern, $offset);
        $this->matchLength = mb_strlen($pattern);
    }

    /**
     * returns one code for a given pattern
     *
     * @return string
     */
    public function generateCode(): string
    {
        if (strlen((string)++$this->lastCode) > $this->matchLength) {
            $this->lastCode = 0; // we don't have any space left, wrap back to 0
        }
        return str_pad($this->lastCode, $this->matchLength, '0', STR_PAD_LEFT);
    }

    /**
     * must return the maximum amount of unique codes the generator can generate based on the current pattern
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int
    {
        return pow(10, $this->matchLength);
    }
}
