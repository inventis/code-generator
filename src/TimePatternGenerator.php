<?php

namespace Inventis\CodeGenerator;

/**
 * this class uses a variant of TOTP
 * it can be used to try mitigate time based concurrency in your codes
 */
class TimePatternGenerator extends AbstractPatternGenerator
{
    /**
     * match $(2) pattern where 2 is the length to use (min 1, max 99)
     * @var string
     */
    protected static $patternMatcher = '/\$\(\d{1,2}\)/';

    /**
     * returns one code for a given pattern
     *
     * @return string
     */
    public function generateCode(): string
    {
        $timestamp = microtime(true);
        $binaryTimestamp = pack('N*', 0) . pack('N*', $timestamp);
        $hash = hash_hmac('sha1', $binaryTimestamp, md5(uniqid($binaryTimestamp)), true);

        $offset = ord($hash[19]) & 0xf;
        return (
                ((ord($hash[$offset+0]) & 0x7f) << 24 ) |
                ((ord($hash[$offset+1]) & 0xff) << 16 ) |
                ((ord($hash[$offset+2]) & 0xff) << 8 ) |
                (ord($hash[$offset+3]) & 0xff)
            ) % pow(10, $this->getCodeLength() - 1);
    }

    /**
     * must return the maximum amount of unique codes the generator can generate based on the current pattern
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int
    {
        $codeLength = $this->getCodeLength();
        if ($codeLength > 0) {
            // if the length is 2 then we can generate 99 unique codes = 10^2=100-1=99
            return 10 ** $codeLength - 1;
        };
        return 0;
    }

    /**
     * @return int
     */
    private function getCodeLength(): int
    {
        if (preg_match('/\d+/', $this->getPattern(), $matches) === 1) {
            return $matches[0];
        }
        return 0;
    }
}
