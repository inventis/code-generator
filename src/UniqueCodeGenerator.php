<?php

namespace Inventis\CodeGenerator;

class UniqueCodeGenerator implements CodeGeneratorInterface
{
    /**
     * @var int
     */
    private $maxRetries;
    private $attempts = 0;
    /**
     * @var CodeGeneratorInterface
     */
    private $generator;
    /**
     * @var ExistingCodeValidatorInterface
     */
    private $existingCodeValidator;

    /**
     * @param CodeGeneratorInterface         $generator a none unique version to use as base implementation
     * @param ExistingCodeValidatorInterface $existingCodeValidator
     */
    public function __construct(
        CodeGeneratorInterface $generator,
        ExistingCodeValidatorInterface $existingCodeValidator
    ) {
        $this->generator = $generator;
        $this->existingCodeValidator = $existingCodeValidator;
        $this->maxRetries = $this->generator->getMaxUniqueCodes();
    }

    /**
     * attempts to generate a unique code, throws an exception when if fails to do so due to exhaustion
     *
     * @return string
     *
     * @throws UniqueCodeExhaustionException when no more codes can be generated
     */
    public function generateCode(): string
    {
        $code = $this->generator->generateCode();

        if ($this->existingCodeValidator->generatedCodeExists($code)) {
            if ($this->attempts < $this->maxRetries) {
                return $this->generateCode();
            }
            throw new UniqueCodeExhaustionException(
                'You have reached max unique codes available for reservations, upgrade code length?!'
            );
        }
        return $code;
    }

    /**
     * returns the maximum amount of unique codes the generator can generate with the current generators
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int
    {
        return $this->maxRetries;
    }

    /**
     * generates a new code on every cast to string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->generateCode();
    }
}
