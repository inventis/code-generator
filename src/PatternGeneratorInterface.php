<?php
namespace Inventis\CodeGenerator;

interface PatternGeneratorInterface
{
    /**
     * @param string $pattern the pattern that should be used for generation
     * @param int    $offset the offset of this match in the collection
     * @throws InvalidPatternException if $pattern is not seen as a valid pattern
     */
    public function __construct(string $pattern, int $offset);

    /**
     * returns one code for a given pattern
     *
     * @return string
     */
    public function generateCode(): string;

    /**
     * returns a list of {@see PatternGeneratorInterface} components matching the specified pattern
     * each match returns a corresponding {@see PatternGeneratorInterface} object that can be used to generate the code
     * if no matching generators can be created the result must be an empty array
     *
     * @param string $codePattern the codePattern
     *
     * @return PatternGeneratorInterface[]
     */
    public static function createFromCodePattern(string $codePattern): array;

    /**
     * returns a single {@see PatternGeneratorInterface} instance that will use the given instance
     *
     * @param string $pattern the pattern that should be used for generation, this MUST be a valid pattern for the given
     *                        {@see PatternGeneratorInterface} instance and if not an exception will be thrown
     * @param int    $offset the offset of this match in the collection, if you are using a single instance of this
     *                       generator you can omit the offset as it should always be 0
     *
     * @throws InvalidPatternException if $pattern is not seen as a valid pattern
     *
     * @return PatternGeneratorInterface
     */
    public static function createFromPattern(string $pattern, int $offset = 0): PatternGeneratorInterface;

    /**
     * return the offset in the collection this {@see PatternGeneratorInterface} object was created for
     * which is useful for mapping purposes etc.
     *
     * @return int
     */
    public function getOffset(): int;

    /**
     * returns the pattern that this object will generate codes from
     *
     * @return string
     */
    public function getPattern(): string;

    /**
     * must return the maximum amount of unique codes the generator can generate based on the current pattern
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int;
}
