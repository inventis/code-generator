<?php
namespace Inventis\CodeGenerator;

/**
 * Class StaticAlphaNumericCodeGenerator
 *
 * this generates a static code that is the upper-cased version of its match useful for when you want to allow user
 * defined characters in a pattern string
 *
 * @package Inventis\CodeGenerator
 */
class StaticAlphaNumericPatternGenerator extends AbstractPatternGenerator
{
    /**
     * match an alphanumeric character
     * @internal do not use \w as that matches an additional `_`
     * @var string
     */
    protected static $patternMatcher = '/[a-zA-Z0-9\-]+/';
    /**
     * @var int the length of the pattern matches
     */
    protected $matchLength;

    public function __construct(string $pattern, int $offset)
    {
        parent::__construct($pattern, $offset);
        $this->matchLength = mb_strlen($pattern);
    }


    /**
     * returns one code for a given pattern
     *
     * @return string
     */
    public function generateCode(): string
    {
        return mb_strtoupper($this->getPattern());
    }

    /**
     * must return the maximum amount of unique codes the generator can generate based on the current pattern
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int
    {
        return 1;
    }
}
