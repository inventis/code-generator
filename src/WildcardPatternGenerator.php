<?php
namespace Inventis\CodeGenerator;

/**
 * Class QueryPatternGenerator
 *
 * This object will look for `*` strings on the code pattern provided and will generate
 * a single alphanumeric character as a code
 * @package Inventis\CodeGenerator
 */
class WildcardPatternGenerator extends AbstractPatternGenerator
{
    /**
     * match a single *
     * @var string
     */
    protected static $patternMatcher = '/\*/';

    protected static $minChar = 48;
    protected static $maxChar = 90;

    protected static $validation = '/[0-9A-Z]/';

    /**
     * returns one code for a given pattern
     *
     * @return string
     */
    public function generateCode(): string
    {
        $char = chr(mt_rand(static::$minChar, static::$maxChar));// try to generate from ascii
        if (!preg_match(static::$validation, $char)) {
            return $this->generateCode(); // try again as a special char was matched (about 12 in the set)
        }
        return $char;
    }

    /**
     * must return the maximum amount of unique codes the generator can generate based on the current pattern
     *
     * @return int
     */
    public function getMaxUniqueCodes(): int
    {
        return 36; // 10 digits + 26 characters
    }
}
