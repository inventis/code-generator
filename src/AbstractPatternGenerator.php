<?php
namespace Inventis\CodeGenerator;

abstract class AbstractPatternGenerator implements PatternGeneratorInterface
{
    /**
     * a regex pattern that will be used to strip a code part from a string
     *
     * @var string
     */
    protected static $patternMatcher = '/./';

    /**
     * @var string
     */
    private $pattern;

    /**
     * @var int the offset within a collection
     */
    private $offset;

    /**
     * @param string $pattern the pattern that should be used for generation
     * @param int    $offset the offset of this match in the collection
     *
     * @throws InvalidPatternException if $pattern is not seen as a valid pattern
     */
    public function __construct(string $pattern, int $offset)
    {
        $this->setPattern($pattern);
        $this->setOffset($offset);
    }

    /**
     * returns a list of {@see PatternGeneratorInterface} components matching the specified pattern
     * each match returns a corresponding {@see PatternGeneratorInterface} object that can be used to generate the code
     *
     * @param string $codePattern the codePattern
     *
     * @return PatternGeneratorInterface[]
     */
    public static function createFromCodePattern(string $codePattern): array
    {
        $list = [];
        if (preg_match_all(static::$patternMatcher, $codePattern, $matches, PREG_PATTERN_ORDER | PREG_OFFSET_CAPTURE)) {
            foreach ($matches[0] as $match) {
                $list[] = static::createFromPattern($match[0], $match[1]);
            }
        }
        return $list;
    }

    /**
     * returns a single {@see PatternGeneratorInterface} instance that will use the given instance
     * @param string $pattern the pattern that should be used for generation, this MUST be a valid pattern for the given
     *                        {@see PatternGeneratorInterface} instance and if not an exception will be thrown
     * @param int    $offset the offset of this match in the collection, if you are using a single instance of this
     *                       generator you can omit the offset as it should always be 0
     *
     * @throws InvalidPatternException if $pattern is not seen as a valid pattern
     *
     * @return PatternGeneratorInterface
     */
    public static function createFromPattern(string $pattern, int $offset = 0): PatternGeneratorInterface
    {
        return new static($pattern, $offset);
    }

    /**
     * sets and validates the pattern that should be generated for against static::$patternMatcher
     *
     * @param string $pattern
     *
     * @return $this
     */
    protected function setPattern(string $pattern): self
    {
        $matches = [];
        // verify it matches the pattern and does not match it more then once as that would require multiple instances
        if (!preg_match_all(static::$patternMatcher, $pattern, $matches, PREG_PATTERN_ORDER | PREG_OFFSET_CAPTURE)
            || (count($matches[0]) !== 1)
            || $matches[0][0][1] !== 0) {
            $matcher = static::$patternMatcher;
            throw new InvalidPatternException(
                "The pattern provided [{$pattern}] is not a valid pattern for this generator [{$matcher}]."
            );
        }
        $this->pattern = $pattern;
        return $this;
    }

    protected function setOffset(int $offset): self
    {
        $this->offset = (int) $offset;
        return $this;
    }

    /**
     * returns the pattern that this object will generate codes from
     *
     * @return string
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * return the offset in the collection this {@see PatternGeneratorInterface} object was created for
     * which is useful for mapping purposes etc.
     *
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
}
